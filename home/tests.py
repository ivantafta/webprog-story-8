from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
from .views import *

# Create your tests here.

class test(TestCase):
    def test_response(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_is_index_used(self):
        found = resolve('/')
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

class functional_test(LiveServerTestCase):

    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    def tearDown(self):
        self.driver.quit()

    def test_index_title(self):
        self.driver.get(self.live_server_url)
        self.assertIn("Story 8", self.driver.title)

    def test_press_up(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("up4").click()
        card = self.driver.find_element_by_xpath("//div[@id='mainAccordion']/div[@class='card'][3]")
        self.assertIn("card4", card.get_attribute("id"))

    def test_press_down(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("down1").click()
        card = self.driver.find_element_by_xpath("//div[@id='mainAccordion']/div[@class='card'][2]")
        self.assertIn("card1", card.get_attribute("id"))

    def test_theme_change(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("theme-change").click()
        curr = self.driver.find_element_by_xpath("//div[@id='mainc']")
        self.assertIn("bg-dark", curr.get_attribute("class"))